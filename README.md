# Lebensmitteldatenbank mit C++ Microservice und Typescript

Projekt um Datenbanken wieder reinzukommen und im Zusammenhang erste Erfahrungen mit Typescript zu erhalten.

## Welche Informationen müssen in der Datenbank gespeichert werden?
* Barcode
* Name
* Anzahl
* Zutatenliste
* Kalorien?
* Was? (Beschreibung wenn vorhanden)
* Hersteller
* Wo wurde es verstaut (Keller, Vorratsschrank, Other mit Textfeld)

## Welche Technologien willst du einsetzen?
* C++ im Backend
* MySQL Datenbank
* Typescript im Frontend
* Vielleicht auch React
* EAN Barcode Libary
    - https://www.opengtindb.org/api.php
* Open Food Facts (kann mit EAN gesucht werden)
    - https://de.openfoodfacts.org/data
* Quagga Barcode Reader Libary
    - https://serratus.github.io/quaggaJS/
